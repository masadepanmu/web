<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<body>
    <h1>Buat akun baru!</h1>
    <h3>Form Pendaftaran</h3>    
    <form action="/welcome" method="post">
        @csrf
        <p>Nama depan :</p>
        <input name="namadepan">
        <p>Nama belakang :</p>
        <input name="namabelakang">
        <p>Jenis Kelamin :</p>
        <input type="radio" name="jenis_kelamin" value="Laki-Laki" /> Laki-Laki</br>
        <input type="radio" name="jenis_kelamin" value="Perempuan"> Perempuan</br>
        <input type="radio" name="jenis_kelamin" value="Lainnya"> Lainnya</br>
        <p>Negara :</p>
        <select name="bahasa" id="">
            <option value="Indonesia">Indonesia</option>
            <option value="Inggris">England</option>
        </select>
        <p>Berbahasa :</p>
        <input type="checkbox" name="bicara" value="Bahasa Indonesia">Bahasa Indonesia</br> 
        <input type="checkbox" name="bicara" value="Bahasa Inggris">English </br>  
        <input type="checkbox" name="bicara" value="-">Other
        <p>Tentangmu : </p><br>
        <textarea name="isi" id="" cols="30" rows="10"></textarea>
        </p>
        <input type="submit" value="Daftar">
        <!--<button type="submit" onclick="/signUp">Sign Up</button>
    </form>
</body>
</html>
