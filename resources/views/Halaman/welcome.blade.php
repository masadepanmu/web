<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>SELAMAT DATANG!</h1>
    <h3>Terima kasih telah bergabung di Sanberbook, {{$frontname}}. Social Media kita bersama!</h3><br>
    <h2>Profile</h2>
    <P>Nama Lenkap Anda : {{$frontname}} {{$backname}}</P><br>
    <P>Jenis Kelamin : {{$gender}}</P><br>
    <P>Negara : {{$country}}</P><br>
    <P>Berbahasa : {{$spoken}}</P><br>
    <P>Tentang Anda : {{$bio}}</P><br>
</body>
</html>