<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('Halaman.register');
    }

    
    public function welcome(Request $request)
    {
        $frontname = $request['namadepan'];
        $backname = $request['namabelakang'];
        $gender = $request['jenis_kelamin'];
        $country = $request['bahasa'];
        $spoken = $request['bicara'];
        $bio = $request['isi'];
        
        return view('Halaman.welcome', compact('frontname', 'gender', 'country','spoken','bio','backname'));
    }
}

